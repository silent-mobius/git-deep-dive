
---
# Create local repository

Now that we have Git installed and configured let's create our first Git project. What this means is that all files in this directory will be under Git revision control and those files could be tracked by Git. Since I'm doing this on Linux I'm going to create a directory in my user's home directory called code. My preference is to save all my code projects in `Projects` folder. So lets create one get into it:

```ini
aschapelle@vaiolabs.io:$ mkdir -p ~/Projects/code
aschapelle@vaiolabs.io:$ cd ~/Projects/code
```
Now we're going to initialize the repository. 
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git init
```
We get a message saying that the Git project has been initialized, and it mentions a `.git` directory, that we discussed in past chapters. 

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git init
aschapelle@vaiolabs.io: ~/Projects/code $ git clone
```
---

# Adding and committing files

Having a Git Repository isn't useful if don't have anything to keep in it. So, we should create a file and work with git to manage its versions:

```ini
touch init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git status
aschapelle@vaiolabs.io: ~/Projects/code $ git add init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git status
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -m "initial commit"
aschapelle@vaiolabs.io: ~/Projects/code $ git status
```

---
# Adding and committing files (cont.)

The command above are referred as basic workflow for Git:
 - Added a file
 - Stage it to be committed by adding it
 - And then committing the changeset.

---

# Adding and committing files (cont.)

We'll do another commit, but this one we'll do in a bit different way: 
- We'd add line into file.

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ echo "# this is a comment" > init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git status
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -a # this will commit all the changes of the file and will open editor because we didn't used -m option for adding commit message.
```

---

# Adding and committing files (cont.)

Few words about commit messages:

- You can be verbose, but keep it within 50 characters
- No need for a date or version information
- Notice the wording in the commit messages. They're in present tense and not in past tense. This commit changes something. 
- Remember that Git stores changes, not versions. 
- Generally, you want a short line with a summary about the change does. Followed by a blank line and a more descriptive message. 
- Be descriptive as you will be looking at the messages later. A message that says "Fixes big" tells us nothing.
  -  A better message will tell you which issue was fixed and maybe even reference the bug tracker number. 


<!-- modify -->

---

# Git commit history

Now that we have some commits in our Git project, let's take a look at our Git log. 
```git
aschapelle@vaiolabs.io: ~/Projects/code $ git log
```

Same output can be presented in short by adding `--oneline` option
```git
aschapelle@vaiolabs.io: ~/Projects/code $ git log --oneline
```
---

# Git commit history (cont.)

We can limit the output of Git log. The syntax is similar to the head and tail command. 
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git log -n 1
```

We can also specify a date. If we want to view all commits since a certain date, we can specify it. My commits were all done on May 15th, 2019, so if I want to show them, I'd type in git log `--since` equals 2019-05-14 which will be yesterday. This could be useful if you come into work and pull down the latest commits to a project. 
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git log --since
```
---

# Git commit history (cont.)

You can also change `--since` to `--until` and it will show you all commits up a point. This may be useful if the software broke at a certain time and we need to look at the commits up until that time. 
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git log --until
```
We can also search for all commits by a certain author. For instance, in my case I type in clear and then git log `--author`. Git does a loose search for the author, so you don't have to be exact.
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git log --author=silent-mobius
```
---

# Git commit history (cont.)
You can also search for certain commits based on text. This simulates the grep command in Linux and Unix. If you wanted to search for a commit message containing the word nothing, we'd type in git log `--grep` equals double quote nothing double quote and hit Enter.
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git log --grep something --author
```
If you want more information on using basic and extended regular expressions, search the course library for Bash Pattern Matching over Google or some other course by me at [gitlab profile](https://gitlab.com/silent-mobius) 

---
# Ignoring files

There may be times when you want to store files in the Git project directory, but you don't want Git to track them. 
for example lets add some files to our projects:

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ touch my_song.mp3 your_pic.png example.txt
```
Git won't automatically track them, but it will warn you every time you do a commit that there are untracked files. What we really want is for Git to ignore the files completely. If we've decided not to store binary files in our Git project, we may want to ignore those.

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ touch .gitignore
aschapelle@vaiolabs.io: ~/Projects/code $ echo '*.mp3' 'example.*' \
'your_pic.png' >> .gitignore
```
This is really useful if using Git to track non-traditional files like operating system files, as we are. To finish , you may want to commit these changes to keep your git status messages.

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git show force
```
---

# Rolling back changes

Before diving into rolling back our git error, one thing must be noted: we haven't yet covered all topics of **git workflow** which are kind of branching strategy of maintained by git:

<img src='../misc/.img/git-workflow.png' alt=diagram style="float:right;width:300px;">

- Work Dir: holds actual files of code.
  - add : creating new files without adding them for git follow them, by running `git add .`
- Index(stage): acts as staging area
  - committing: running `git commit -m "message"` for git to add the code to HEAD
- HEAD: git variable for referencing(pointing) to latest checkout branch
---
# Rolling back changes (cont.)

Whenever you work on your project or configuration files, you might add incorrectly saved files to git version control. here we'll demonstrate how to **"cancel"** those changes within git workflow.

Let's start with example, by setting init_script.sh as bash script:

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ echo '#!/usr/bin/env bash ' \
> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ echo -e " \n" >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ echo "echo \"H311o W0rRld\' " \
>> init_script.sh
```

---
# Rolling back changes (cont.)

Now lets go through git flow steps :

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git add .
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -m "adding initial script"
aschapelle@vaiolabs.io: ~/Projects/code $ git status
aschapelle@vaiolabs.io: ~/Projects/code $ git log 
```
Last two commands prints that status of file and latest commit.

---
# Rolling back changes (cont.)
Now let mess it up:
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ echo 'eco  "this is script name: $0"' >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git status # to show that the file hasn't been added to git yet
aschapelle@vaiolabs.io: ~/Projects/code $ git diff # to show the difference between versions of file
```
Due to a fact that git isn't following the code e can revert it with `git checkout -- <filename>`
in our case:

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout -- init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ cat init_script.sh # to see that line was removed
```
Essentially `git checkout` is used to change from branch to branch, yet when `--` is provided it reverts to current branch, thus changing to latest change on current branch

---
# Rolling back changes (cont.)

Now let's repeat all the steps, but also add the file to staging(index) in git workflow.

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ echo 'eco  "this is script name: $0"' >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git add init_script.sh 
aschapelle@vaiolabs.io: ~/Projects/code $ git status # to show you that file has been added to staging
```
Due to printout that `git add` can be canceled with `git reset` command, lets implement in our case:
```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git reset HEAD init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git status # to show that it has been removed from staging
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout -- init_script.sh # to revert the changes to what it was before hand
```
Wasn't it fun ?
---
# Rolling back changes (cont.)

But what if we committed it to HEAD ?


```ini
aschapelle@vaiolabs.io: ~/Projects/code $ echo 'eco  "this is script name: $0"' >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git add init_script.sh 
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -m "adding error code"
aschapelle@vaiolabs.io: ~/Projects/code $ git log -n1
aschapelle@vaiolabs.io: ~/Projects/code $ git revert 73e7df749d52c3e20e9aa37062ee708a71f9a5e5 # sha1 one from git log -n1
```

---
# Rolling back changes (cont.)

 all cool and stuff, but what if I just need to change commit message?

```ini
aschapelle@vaiolabs.io: ~/Projects/code $ git commit --amend

```
---

# Summary
