

---

# Remote repository

Remote repositories are versions of your project that are hosted on the Internet or network somewhere.
Collaborating with others involves managing these remote repositories and pushing and pulling data to and from them when you need to share work.
Managing remote repositories includes knowing how to add remote repositories, remove remotes that are no longer valid, manage various remote branches and define them as being tracked or not, and more. 
In this chapter, we’ll cover some of these topics.


---
# Create GitLab account

In order to have examples for our course , it is suggested to use some type of remote git repository. The most popular of them all are:
- [GitHub](https://github.com)
- [GitLab](https://gitlab.com)
- [Bitbucket](https://bitbucket.com)

All the 3 of them are free to sign-up and their working plan might change within time. 
For purposes of our course it doesn't matter to which you sign-up, yet it is significant to mentioned that this course project is managed over [GitLab](https://gitlab.com). We'll be providing examples from there, yet you are welcome to create at **any** those sites as well as create accounts on **all** of them.

Thus lets create accounts for each and everyone one of you

<!-- demo creating the account -->
<!-- demo creating repository -->
---

# Cloning a remote GitLab repository

At this point we have created account and first project.

Now lets clone it.  lets go to the page of your page clone it to different folder.
```sh
aschapelle@vaiolabs.io:~$ git clone  https://gitlab.com/code
```
or
```sh
aschapelle@vaiolabs.io:~$ git clone  https://gitlab.com/code /path/to/some/different/folder
```
There is a possibility to clone the code to designated folder, by adding path to that folder at the end of the line.

---
# Cloning a remote GitLab repository(cont.)

When cloning repository, you can either choose SSH or HTTPS protocol for git to transfer data to gitlab/gitlab/bitbucket and so on. 
![](misc/.img/git-clone.png)

We'll go with HTTPS for now.

Copy the URL onto your clipboard, and then go back to your terminal. In your terminal
`git clone URL` where `URL` is the link that you have used.
Due to HTTPS being the protocol that we used, authentication is required, so enter your username and password when prompted while cloning. Now verify by typing in ls. You should see a `code` directory. Change into project folder and `ls` again. All of the files should be the same as in our original project. The last thing we'll do here is check our remotes.

`git remote -v` will show that remotes are already setup for you.

Now you're ready to contribute to the `code` on the new host. 

That's it ... no strings attached :smily:

---

# Getting and Pushing to a remote repository

When creating a local repository, we'll eventually need to save it, so we `push` changes to remote repository. The Flow for that process would be as follows:
```sh
aschapelle@vaiolabs.io:~$ cd ~/Project/code
aschapelle@vaiolabs.io:~$ git init
aschapelle@vaiolabs.io:~$ git config user.name alex.schapelle
aschapelle@vaiolabs.io:~$ git config user.name alex@vaiolabs.com
aschapelle@vaiolabs.io:~$ git remote add origin https://gitlab.com/alex.schapelle/code.git
aschapelle@vaiolabs.io:~$ git add .
aschapelle@vaiolabs.io:~$ git commit -m "adding initial commit"
aschapelle@vaiolabs.io:~$ git push -u origin master
```
---


# Getting and Pushing to a remote repository (cont.)

Yet, We usually do not work on the project alone, meaning that not just us are cloning that same project others as well. This is what is called **collaborative work** on project. In case it was not clear, many people `push` code to our project, but we'll never know it until we update our code by `pull`ing it from remote server.

To do so, we can `cd` in project and run `git pull`. The command will pull all the other `push`es from other developers on to the shared branch that we are working on together.

Another point in regards to **collaborative work** : when more then one person is working on the branch, whether it is `master`/`main` or any other branch, it is best practice to run `git pull` every time, before one `push`es her/his code. In terms of flow it would look some thing like this:

```sh
aschapelle@vaiolabs.io:~$ cd ~/Project/code
aschapelle@vaiolabs.io:~$ git pull
aschapelle@vaiolabs.io:~$ echo "echo 'updating some code'" >> README.md
aschapelle@vaiolabs.io:~$ git add .
aschapelle@vaiolabs.io:~$ git commit -m "updating README.md"
aschapelle@vaiolabs.io:~$ git push # after remote is established there is no need to add -u origin main
```

Same would be in case of branches:

```sh
aschapelle@vaiolabs.io:~$ cd ~/Project/code
aschapelle@vaiolabs.io:~$ git checkout -b somebranch
aschapelle@vaiolabs.io:~$ git pull -u origin master  \
# in this case we are updating main branch, and its effect will be transfered to somebranch
aschapelle@vaiolabs.io:~$ echo "echo 'updating some code'" >> README.md
aschapelle@vaiolabs.io:~$ git add .
aschapelle@vaiolabs.io:~$ git commit -m "updating README.md"
aschapelle@vaiolabs.io:~$ git push
```

---

# Create a local git server and repository

Although not always, It useful to know how setup `bare` git server with only just `git` command.
Before github/gitlab, this was acceptable way for small to medium companies to work on their code release.

```sh
aschapelle@vaiolabs.io:~$ useradd -d /srv/git git  
aschapelle@vaiolabs.io:~$ passwd git
aschapelle@vaiolabs.io:~$ mkdir -p /srv/git/Projects.git
aschapelle@vaiolabs.io:~$ cd /srv/git/Projects.git
aschapelle@vaiolabs.io:~$ sudo git init --bare
aschapelle@vaiolabs.io:~$ cd /srv
aschapelle@vaiolabs.io:~$ git chown -R git.git git
```
- Yes, for every repository you'll have to create its own **folder** with extension `.git`
- Yes, in case it was not created with git user himself, you'll need to `chown` the permissions
- No, no need to create new user every time we want new repository
---

# Create a local git server and repository 

## (cont.)

Today we have more then `git --bare` server under our own capabilities and I'd like to encourage you to install and test options below:

- [GitLab](https://about.gitlab.com/install/)
- [Gitea](https://docs.gitea.io/en-us/install-from-package/)

Both of these are great options and can be met at various project of OSS and others.

---
# Using SSH keys for authentication 

As mentioned in previous chapter, we have used `HTTPS` protocol to transfer data to git server. Yet other options also exists, most common of them being  `SSH` protocol. The main reason for its use case being, by passing step authentication with **ssh-keys**.

In order to set it up we'll need to:
- Generate ssh-key with `ssh-keygen` command
- Copy public key to remote server
- Change ssh config in `~/Projects/code/.git/config` file
- Authorize the ssh-key digital finger-print

---

# Using SSH keys for authentication (cont.)

### Generate ssh-key with `ssh-keygen` command

Just run command below

```sh
aschapelle@vaiolabs.io:~$ssh-keygen -t rsa -b 2048 \
 # -t: type -b: byte size of encryption
```
The command above should prompt us with several question like password and location of ssh-keys, to which I'd to suggest to leave empty and press `Enter` key.
In case to which you already have default ssh-key names used for something else, I'd suggest to use the `Public` portion for the git authentication, or just generate file with different name.

- **[!] WARNING**: There are some cases when, using ssh-key with other names then `id_rsa.pub` might require you to update git commands environment variable `GIT_SSH_COMMAND`**every time you use `git push/pull`**

As an example:

```sh 
aschapelle@vaiolabs.io:~$ GIT_SSH_COMMAND="ssh -i ~/.ssh/my_key.pub" git clone example
```

---

# Using SSH keys for authentication (cont.)

### Copy public key to remote server
In case you are using gitlab, open your account and go to preferences

<img src='misc/.img/gitlab-preferance.png'>


In there choose `SSH Keys`
<img src='misc/.img/gitlab-ssh-key.png)


And at key area paste the content of your `id_rsa.pub` file.
![](misc/.img/key.png)

Save the change.

---

# Using SSH keys for authentication (cont.)
### Change ssh config in `~/Projects/code/.git/config` file

Before run `git push` command it is suggested to edit the project configuration file. Just go to your project file and  change `url` of remote server as from this:

```sh
[remote "origin"]
	url = https://gitlabs.com/alex.schapelle/code.git
```

to this:

```sh
[remote "origin"]
	url = git@gitlab.com:alex.schapelle/code.git
```

- **[!] WARNING**: Do NOT change any other file under `remote` config.

---

# Using SSH keys for authentication (cont.)
### Authorize the ssh-key digital finger-print

All is left to authorize the ssh key and to do so you only need to try and use `git push/pull` command.

---

# Summary
