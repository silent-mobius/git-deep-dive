
---
# What is Version Control

In software engineering, version control, also known as revision control, source control, or source code management, is a class of systems responsible for managing changes to computer programs, documents, large web sites, or other collections of information. Version control is a component of software configuration management.

---

# What is git?

Git is software for tracking changes in any set of files, usually used for coordinating work among programmers collaboratively developing source code during software development. Its goals include speed, data integrity, and support for distributed, non-linear workflows (thousands of parallel branches running on different systems).

---
# What is git? (cont.)

Git was created by Linus Torvalds in 2005 for development of the Linux kernel, with other kernel developers contributing to its initial development. Since 2005, Junio Hamano has been the core maintainer. As with most other distributed version control systems, and unlike most client–server systems, every Git directory on every computer is a full-fledged repository with complete history and full version-tracking abilities, independent of network access or a central server. Git is free and open-source software distributed under the GPL-2.0-only license. 

---
# What is git? (cont.)
Torvalds sarcastically quipped about the name git (which means "unpleasant person" in British English slang): "I'm an egotistical bastard, and I name all my projects after myself. First 'Linux', now 'git'."
---
# What is git? (cont.)

The man page describes Git as "the stupid content tracker". The read-me file of the source code elaborates further:

```docker
"git" can mean anything, depending on your mood.

- Random three-letter combination that is pronounceable, and not actually used
by any common UNIX command. The fact that it is a mispronunciation of "get" may 
or may not be relevant.
- Stupid. Contemptible and despicable. Simple. Take your pick from
 the dictionary of slang.
- "Global information tracker": you're in a good mood, and it actually works for you.
 Angels sing, and a light suddenly fills the room.
- "Goddamn idiotic truck-load of sh*t": when it breaks.
```


---

# How does git differ?

As mentioned several times, git is distributed version control system:

- Each user has his own local repository
- Changes can be shared but it is not demanded
- Changes can be stored remote repository but it is not demanded
- All is saved with git:
    - Text files
    - Bin files (although not perfect)
    - Large files (file size of several gigabytes)
- Changes are saved not version of change


---

# Why use git ?

- Multiple uses cases:
    - System administration
    - QA automation code
    - Software development version control
    - Office suite

---

# Why use git ? (cont.)

- Binary file storing:
    - Needs some discussion
        - Whole binary is saved
        - Cannot merge bin files
        - Remote repository might need lot of space to save it

---

# Why use git ?

- Distributed (Does not need a central server)
- Very fast local versioning
- Network is not necessary
- \#Opensource
- Very popular

---

# Summary

covered several subjects like,
- what is git.
- who uses it.
- why to use it.

