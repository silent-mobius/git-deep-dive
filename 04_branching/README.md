
---
# Why use branches ?

Branches are used to develop features isolated from each other. The master/main branch is the "default" branch when you create a repository. Use other branches for development and merge them back to the `main` branch upon completion. 

In theory branches work as follows:
- Create a test copy of all files in project
- Make changes
- Test changes
- Merge change into master/main branch

---

# Creating branches

We start checking the current branch name we are working on :
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git branch
```
The output mostly either will be `main` or `master` in case you are using older version of git
In case where you'd like to create a new branch, you'd just run same command with named brach of your desire:
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git branch test_script_branch
```
---
# Creating branches (cont.)

To hop on to new branch you'd usually use `checkout` subcommand, as shown below:
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout test_script_branch
```
Or in case you are lazy like me and would like to have shorter alternatives, you could create a branch and also change to it with one simple command:
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout -b test_script_branch
```
---
# Creating branches (cont.)

We can verify your current branch with `git branch` command.

- `[!]` Note: it is good practice to configure your prompt with git command to let you know on which branch you are located in order to avoid errors.
- `[!]` Please add `PS1='\[\033[01;32m\]\u@\h \[\033[00m\]\w\[\033[01;34m\] [$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]$ '` 
to your local `.bashrc` file and log into new session of your shell
- `[!]` Note: creating local branch, **does NOT** mean that the same branch is created on remote server. will be further discussed on next chapter

---
# Creating branches (cont.)

We can create some new files on this branch and verify that they are not moving to older branch:

```sh
aschapelle@vaiolabs.io: ~/Projects/code $ touch second_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git add second_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -m "adding second script"
aschapelle@vaiolabs.io: ~/Projects/code $ git log
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout main
aschapelle@vaiolabs.io: ~/Projects/code $ git log 
aschapelle@vaiolabs.io: ~/Projects/code $ ls -l
```
---

# Branch actions

## comparing renaming and deleting

In the process of making changes to system configurations, we can analyze the git logs to see what the changes have been between commits. But with branches It's not useful because each branch has its own log. Git provides us with solution for this situation. We can diff the two branches to see what the differences are:
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git diff main..test_script_branch
```
---
# Branch actions (cont.)

## comparing renaming and deleting


We can compare previous version of our branch, we can just append a caret `^` to your`test_script_branch` name. What this is doing is comparing the previous version of the `test_script_branch` branch or the version right before the last commit and not the most recent version
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ compare git diff main..test_script_branch^
```
Due to previous version being the same as main branch changes will not be shown

---
# Branch actions (cont.)

## comparing renaming and deleting

In Linux, we rename files by `mv-ing` them to the new name. With git, we rename branches the same way. This can also be shortened to `-m`. 
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git branch --move test_script_branch dev_branch
```
or
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git branch -m test_script_branch dev_branch 
```

---
# Branch actions (cont.)

## comparing renaming and deleting

There are cases when branch that you've been working on, is not in use anymore or is not needed. In those cases it is usually require to delete branch:
```sh 
 aschapelle@vaiolabs.io: ~/Projects/code $ git branch --delete dev_branch
```
or

```sh
 aschapelle@vaiolabs.io: ~/Projects/code $ git branch -d dev_branch 
```
> Note: to delete a branch, you need to be on a **different** branch, from the one you are deleting.

---
# Branch merging 

After you've made changes to files on `dev_branch`, and you want to merge them into `main` branch . For this we'll need to merge the branches
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git branch # verify that you are on dev_script branch and if not then create it and checkout to it
aschapelle@vaiolabs.io: ~/Projects/code $ echo -e "\necho \"Goodbye World\"" >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git -a -m "adding another code to script"
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout main # we must change to main branch or any other branch in order to merge one branch with other
aschapelle@vaiolabs.io: ~/Projects/code $ git merge dev_script
```
This type of merging is also called **fast-forward** merge
Easy, right ?!

---

# Branch merging (cont.)

Not that fast !!!
Merge can be fun ... unless there are **conflicts** (imagine dramatic music).
- Conflicts are a situation when a same file on several  branches, on specific line, has different value of code.

Lets setup an example:
- We should create different code on two branches on the same line
```sh
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout dev_script # change to dev_script branch if you are not on it.
aschapelle@vaiolabs.io: ~/Projects/code $ echo -e "\necho \"third line\"" >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -a -m "adding another code to script"
aschapelle@vaiolabs.io: ~/Projects/code $ git checkout main
aschapelle@vaiolabs.io: ~/Projects/code $ echo -e "\necho \"3rd line\"" >> init_script.sh
aschapelle@vaiolabs.io: ~/Projects/code $ git commit -a -m "adding another code to script"
aschapelle@vaiolabs.io: ~/Projects/code $ git merge main dev_script # this should create conflict
```

---

# Branch merging (cont.)

Aww!!! Thats amazing! Now, how do we fix it ? Easy:
Just remove what you **Do NOT Want** and leave the desired code.

> Note: In case of having some type of special editor like Atom, VScode, Geany or specialized IDE like InteliJ, Pycharm, RubyMine and so on, git will present you with interactive way to fix the conflicts.

---

# Summary


